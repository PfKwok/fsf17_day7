/**
 * Server side code.
 */
"use strict";
console.log("Starting...");
var express = require("express");
var bodyParser = require("body-parser");
var moment = require("moment");
moment().format();

var app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

console.log(__dirname);
console.log(__dirname + "/../client/");
const NODE_PORT = process.env.PORT || 4000;

app.use(express.static(__dirname + "/../client/"));

app.post("/api/register", (req, res) => {
    var registeredUser = req.body;
    console.log("Email > " + registeredUser.email);
    console.log("Password > " + registeredUser.password);
    console.log("Confirm Password > " + registeredUser.confirmpassword);

    console.log("Date of birth > " + registeredUser.dob);
    var newDob = moment(registeredUser.dob);
    console.log(newDob.toLocaleString());

    res.status(200).json(registeredUser);
});

/*
app.post("/submit-quiz", function (req, res) {
    console.log("Received user object " + req.body);
    console.log("Received user object " + JSON.stringify(req.body));
    var quiz = req.body;
    var checking = quizes[quiz.id];
    if (checking.correctanswer == parseInt(quiz.value)) {
        console.log("CORRECT !");
        quiz.isCorrect = true;
        score++;
    } else {
        console.log("INCORRECT !");
        quiz.isCorrect = false;
    }
 */


app.use(function (req, res) {
    res.send("<h1>Page not found!</h1>");
});

app.listen(NODE_PORT, function () {
    console.log("Web App started at " + NODE_PORT);
});


//make app public
module.exports = app