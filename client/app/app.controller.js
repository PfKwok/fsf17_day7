/**
 * Client side code.
 */


(function () {
    "use strict";
    angular.module("RegApp").controller("RegCtrl", RegCtrl);
    RegCtrl.$inject = ["$http"];

    function RegCtrl($http) {
        var regCtrlself = this;

        regCtrlself.onSubmit = onSubmit;
        regCtrlself.initForm = initForm;
        regCtrlself.onlyFemale = onlyFemale;

        regCtrlself.user = {

        }

        regCtrlself.nationalities = [
            { name: "Please select", value: 0},
            { name: "Singaporean", value: 1},
            { name: "Australian", value: 2},
            { name: "Japanese", value: 3},
            { name: "American", value: 4},
            { name: "Malaysian", value: 5}
        ];

        function initForm (){
            regCtrlself.user.selectedNationality = "0";
            regCtrlself.user.gender = "F";
        }

        function onReset (){
            regCtrlself.user = Object.assign({}, regCtrlself.user);
        }

        function onSubmit (){
            console.log(regCtrlself.user.email);
            console.log(regCtrlself.user.password);
            console.log(regCtrlself.user.confirmpassword);
            console.log(regCtrlself.user.fullName);
            console.log(regCtrlself.user.gender);
            console.log(regCtrlself.user.dob);
            console.log(regCtrlself.user.address);
            console.log(regCtrlself.user.selectedNationality);
            console.log(regCtrlself.user.contactNumber);
            
            $http.post("/api/register", regCtrlself.user).then((result) => {
                console.log("result > " + result);
            }).catch((error) => {
                console.log("error > " + result);
            })
        };

        function onlyFemale (){
//          console.log("only female");
            return regCtrlself.user.gender == "F";
        }

        regCtrlself.initForm();

    }

})();